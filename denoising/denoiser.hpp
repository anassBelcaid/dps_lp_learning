#ifndef DENOISER_H
#define DENOISER_H

/**
*Abstract class for a denoiser;
*/
#include <boost/numeric/ublas/vector.hpp>
#include <boost/numeric/ublas/io.hpp>
namespace ublas=boost::numeric::ublas;
class Denoiser
{
public:
	Denoiser(double lam=10,double h=1)
	{
		setLam(lam);
		setH(h);
	}
	void setLam(double lam)
	{
		this->lam=(lam>0)?lam:10;

	}
	void setH(double h)
	{
		this->h=(h>0)?h:0;
	}
	double getLam()const
	{
		return lam;
	}
	double getH()const
	{
		return h;
	}

	//polymorphic method
    virtual ublas::vector<double> denoise(const ublas::vector<double>& noised)=0;
private:
	double lam;
	double h;
};
#endif