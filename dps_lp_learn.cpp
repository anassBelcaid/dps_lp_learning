#include <iostream>
#include <chrono>
#include "RDps.h"
#include <boost/program_options.hpp>


namespace bo=boost::program_options;
using namespace std;


//string for input and output
string input="";
string output="";
bool verbose=false;

/**
 * @brief prepare_command_line for the executable
 * @param algo : Bpd algorithm
 * @param argc : nombre of argument in
 * @param argv
 * @return
 */
void prepare_command_line(RDps & algo, int argc, char *argv[])
{
    //declaring options
    bo::options_description desc("Options");

    //adding options to desc;
    desc.add_options()
            ("help,h","Print the help message")
            ("version,v","Display the program version")
            ("lambda,l",bo::value<float>()->default_value(20.0f),"Lambda")
            ("sensitivity,H",bo::value<float>()->default_value(8.0f),"Sensitivity")
            ("flatten,F",bo::value<bool>()->default_value(true),"FlattenParts")
            ("inputFile,i",bo::value<string>(&input)->default_value("noised"),"Input file")
            ("ouptutFile,o",bo::value<string>(&output)->default_value("bpd"),"Output fileName")
            ;

    //mapping the options;
    bo::variables_map mp;
    bo::store(bo::parse_command_line(argc,argv,desc),mp);
    bo::notify(mp );

    //processing the command
    if(mp.count("help"))
    {
        cout<<desc<<endl;
        exit(1);
    }
    if(mp.count("version"))
    {
        cout<<mp.at("version").as<string>()<<endl; //to be modified to print the real version
        exit(1);
    }
    algo.setLambda(mp["lambda"].as<float>());
    algo.setH(mp["sensitivity"].as<float>());
    algo.setFlat(mp["flatten"].as<bool>());

    algo.updateAlpha();

}


int main(int argc, char *argv[]) {


    //Creating a b  and representation

    RDps alg(10,9);

    //trying the lines
    try
    {
        //cout<<"processing options"<<endl;
        prepare_command_line(alg,argc,argv);

        //loading the signal

        vec noised;
        noised=alg.loadSignal(input);
        //recovering the signal

        auto t1=std::chrono::steady_clock::now();
        vec denoised=alg.recover(noised);
        auto t2=std::chrono::steady_clock::now()-t1;
        cout << std::chrono::duration <double, std::milli> (t2).count()<<endl;


        //saving the recovered signal
        alg.saveSignal(output,denoised);

    }
    catch (const boost::program_options::error &ex)
    {
        std::cerr << ex.what() << '\n';
    }

}