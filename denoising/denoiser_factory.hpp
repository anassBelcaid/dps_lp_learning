//
// Created by anass belcaid on 28/04/2017.
//

#ifndef LEARNING_DPS_DENOISER_FACTORY_HPP_H
#define LEARNING_DPS_DENOISER_FACTORY_HPP_H
#include "condat_denoise.hpp"
#include "gnc_relaxation.hpp"
class DenoiserFactory
{
public:
    static Denoiser* createCondatDenoiser(double lam=10,double h=0)
    {
        return new Condat_Denoiser(lam,h);
    }

    static Denoiser * createGncDenoiser(double lam=10,double h=0)
    {
        return new Gnc_Denoiser(lam,h);
    }
};
#endif //LEARNING_DPS_DENOISER_FACTORY_HPP_H
