//
// Created by anass belcaid on 28/04/2017.
//

#ifndef LEARNING_DPS_GNC_RELAXATION_H
#define LEARNING_DPS_GNC_RELAXATION_H

#include "denoiser.hpp"

typedef ublas::vector<double> vec;

class Gnc_Denoiser: public Denoiser
{
public:
    virtual vec denoise(const ublas::vector<double> &noised) override {

        int n=noised.size();
        vec output(n);
        iteratifGradientFix(noised,output);
        return output;

    }
    void setPrecision(double prec)
    {
        precision=(prec>0)?prec:1E-4;
    }
    void setIterMax(long iter)
    {
        iterMax=(iter>0)?iter:1E4;
    }
    Gnc_Denoiser(double lam, double h) : Denoiser(lam, h) {


        //setting the program constants
        alpha=(h*h*lam)/2.;
        w=2./(1+1./lam);
        lambda=getLam();


    }

private:
    double alpha;           //penalty for a discontinuity
    double w;               //Relaxation parameter;
    double lambda;
    const double  c=1./2;
    double precision=1E-4;
    double iterMax=1E4;
    //tool functions
    double signe(double p)
    {
        return (p>0)?1:-1;
    }

    void iteratifGradientFix(const vec &D,vec &X0)
    {
        //longeur du vecteur initialiser
        int n=X0.size();
        //initialisation du vectuer à 0
        vec X1(n);

        //iteration 0
        int k=0;

        //Vecteur gradient
        vec grad(n,1);

        int i;

        X1=X0;
        while(ublas::norm_2(grad)>=precision && k<iterMax) {
            //première iterations
            X1[0] = X0[0] - w * (2 * (X0[0] - D[0]) + GPrime(X0[0] - X0[1])) / (2 + 2 * lambda * lambda);

            //iterations sur les éléments intéreurs
            for (i = 1; i < n - 1; i++) {
                X1[i] = X0[i] - w * (2 * (X0[i] - D[i]) + GPrime(X0[i] - X1[i - 1]) + GPrime(X0[i] - X0[i + 1])) /
                                (2 + 4 * lambda * lambda);
            }

            //element à la fin
            X1[n - 1] = X0[n - 1] -
                        w * (2 * (X0[n - 1] - D[n - 1]) + GPrime(X0[n - 1] - X1[n - 2])) / (2 + 2 * lambda * lambda);


            //cacluler le grad
            grad=X0-X1;

            //préparer l'itération suivante
            X0=X1;
            k++;

        }
        //cout<<"convergence at : k= "<<k<<endl;

    }


    double GPrime(double t)
    {
        double min=funQ();
        double max=funR();
        if(fabs(t)>=min && fabs(t)<max)
        {
            return  -c*(fabs(t)-funR())*signe(t);
        }
        else
        if(fabs(t)<funQ())
        {
            return 2*lambda*lambda*t;
        }
        else
        {

            return 0;
        }
    }

    double funG(double t)
    {
        double min=funQ();
        double max=funR();
        if(fabs(t)>=min && fabs(t)<max)
        {
            return  alpha -(c)*pow((fabs(t)-funR()),2);
        }
        else
        if(fabs(t)<funQ())
        {
            return lambda*lambda*t*t;
        }
        else
        {

            return alpha;
        }
    }

    double funR()
    {
        double P=alpha*(4+1./(lambda*lambda));
        return sqrt(P);
    }


    double funQ()
    {
        return alpha/(lambda*lambda*funR());

    }

};

#endif //LEARNING_DPS_GNC_RELAXATION_H
