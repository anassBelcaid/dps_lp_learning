"""
Simple programm to generate a signal to test
"""

import numpy as np


n=2024;
i1=int(n/4);
i2=int(n/2);
i3=int(3*n/4)
sig=np.zeros((n,1));
sig[:i1]=10;
sig[i1:i2]=15;
sig[i2:i3]=20;
sig[i3:]=15;

sig+=np.random.normal(size=sig.shape,scale=2);

np.savetxt("signal",sig,header="%d"%len(sig),comments="",fmt="%.14f");