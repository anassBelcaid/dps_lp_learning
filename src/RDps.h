//
// Created by anass belcaid on 30/04/2017.
//

#ifndef LEARNING_DPS_RDPS_H
#define LEARNING_DPS_RDPS_H
#include <algorithm>
#include <set>
using std::pair;
#include "DPS.h"
#include "condat_denoise.hpp"


class RDps :public Dps {
public:
    RDps(float l, float h1);

    using Dps::updateAlpha;
    vec recover(const vec& noised);
    void setFlat(bool value);
    void setDenoiser( Condat_Denoiser * denoiser);
protected:
    mat C;       //First Cholesky decomposition
    Condat_Denoiser * denoiser;
    bool flatParts;      //retourn pw constants by taking the mean
    std::set<int> possibleLP;
    double denoiserReduction;            //precision of the denoiser on detecting continuous points

    /*------------------------------------------*/
    //tools functions
    vec recover(const vec &noised, int first, int second)const;

    bool split(const vec &noised,int first,int  second,int &pos);
    double add_discontinuity(const vec &considered,int ind)const;

    void flatten(vec & noised);

};


#endif //LEARNING_DPS_RDPS_H
