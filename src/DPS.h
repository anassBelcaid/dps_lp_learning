#ifndef DPS_H
#define DPS_H
#include <boost/numeric/ublas/vector.hpp>
#include <boost/numeric/ublas/matrix.hpp>
#include <boost/numeric/ublas/io.hpp>

#include<iostream>
using std::cout;
using std::endl;
#include <fstream>
using std::ifstream;
using std::ofstream;
#include <string>
using std::string;

namespace ublas=boost::numeric::ublas;
typedef ublas::matrix<double> mat;
typedef ublas::vector<double> vec;
typedef ublas::vector<short> vecs;
class Dps
{
public:
     Dps(float l, float h1){
         lam=l;
         h=h1;
        alpha=(lam*h*h)/2.0;
    }

     void updateAlpha()
     {
         alpha=(h*h*lam)/2.0;
     }
    //recover a noised signal;
    vec recover(const vec &noised );
    void setLambda(float lam1)
    {
        if(lam1>0)
            lam=lam1;
    }
    void setH(float h1)
    {
        //std::cout<<"SEtting the value of h to "<<h1<<endl;
        if(h1>0)
            h=h1;
    }

    vec loadSignal(const string &filename);
    bool saveSignal(const string &filename, const vec & X);
    void flatten(vec &N);

protected:

    //constante of the programe
    float lam;
    float h;               //sensitivity
    float alpha;

    //energy
    double energy(const vec noised, vec X, vecs E)const;
    vec signal_E(const vec &noised, vecs boolean_process)const;
    mat main_matrix(vecs boolean_process, int n)const;

    bool min_level(const vec &noised, vec &X, vecs &E, double &min_energy)const;


    /*-----------linear algebra functions----------------------*/

    vec chol_solve(const mat & M, const vec &b)const;
    mat chol_tridiagonal( const mat &  A)const;
    vec band_fwsv(const mat &M,const vec & b )const;
    vec band_bwsv(const mat  &M,const vec  & b)const;

};

#endif // BPD_H
