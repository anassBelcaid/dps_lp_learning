# DPS algorithm with an unsupervised classifier on the LP

Repository for the **C++** code for implementing the *Discontinuity position Sweep (DPS)* algorithm with a supervised learning over the *Line process(LP)*. The classifier uses a denoiser to get an early estimation on the true signal, and then uses a **thresholding** strategy to classify the continuity processes ie l_i=0.

A first scheme on the code is as follow:

* DPS restorer
   1. Denoiser.
   2. Splitter
      * for every **non Observable** edge:
        * get the associated matrix A.
        * x= solve(A,edge,y)
        *  compute the energy
        *  return the argmin.
    3. I will try to use different solver to accelerate the resolution of the system.

  ## linear solvers for tridiagonal matrices.

  Here I simply collect the two solvers from DPS.
```cpp
    x= solve(A,y,edge);
```

* The first solver dont count for the null value of the *edge* and solve the full matrix.
* The second solver split the matrix **A** by the position of the edge, and solve 2 sub matrices.

