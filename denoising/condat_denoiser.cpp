#include <iostream>
#include <fstream>
using std::cout;
using std::endl;
using std::ifstream;
using std::ofstream;
#include <vector>
using std::vector;
#include <string>
using std::string;
#include <chrono>
#include <boost/program_options.hpp>
namespace bo=boost::program_options;
#include "denoiser_factory.hpp"

//string for input and output
string input="";
string output="";
double lam;
void prepare_command_line(int argc, char const*argv[])
{
    //declaring options
    bo::options_description desc("Options");

    //adding options to desc;
    desc.add_options()
            ("help,h","Print the help message")
            ("version,v","Display the program version")
            ("lambda,l",bo::value<double>(&lam)->default_value(20.0f),"Lambda")
            ("inputFile,i",bo::value<string>(&input)->default_value("noised"),"Input file")
            ("ouptutFile,o",bo::value<string>(&output)->default_value("denoised"),"Output fileName")
            ;

    //mapping the options;
    bo::variables_map mp;
    bo::store(bo::parse_command_line(argc,argv,desc),mp);
    bo::notify(mp );

    //processing the command
    if(mp.count("help"))
    {
        cout<<desc<<endl;
        exit(1);
    }
    if(mp.count("version"))
    {
        cout<<mp.at("version").as<string>()<<endl; //to be modified to print the real version
        exit(1);
    }

    //lam=mp["lambda"].as<double>();
}

int main(int argc, char const *argv[])
{
	prepare_command_line(argc,argv);

	ifstream in;
	in.open(input.c_str());
	int n;
	in>>n;
	ublas::vector<double> signal(n);
    for(auto &v : signal)
        in>>v;

    Condat_Denoiser *denoiser=new Condat_Denoiser(lam,0);

    //-----------------------------------------------------
    //                Denoising
    //-----------------------------------------------------
    auto t1=std::chrono::steady_clock::now();
    auto denoised=denoiser->denoise(signal);
    auto t2=std::chrono::steady_clock::now()-t1;
    cout<<std::chrono::duration<double,std::milli>(t2).count()<<endl;
    ofstream out(output.c_str());
    out<<n<<endl;
    for(auto v: denoised)
        out<<v<<endl;

    //Clean your garbage yourself
    in.close();
    out.close();
    delete denoiser;
	return 0;
}