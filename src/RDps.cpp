//
// Created by anass belcaid on 30/04/2017.
//

#include "RDps.h"
#include <boost/numeric/ublas/vector_proxy.hpp>
#include <queue>
using std::priority_queue;

//structure for line process relation

struct lineEnergy
{
    double energy;
    int lp;
    lineEnergy(int lp, double energy=0):lp(lp),energy(energy)
    {

    };

    bool operator<(const lineEnergy & other)const
    {
        return this->energy>other.energy;
    }
};

typedef pair<int,int> interval;
/**
 * @brief RBpd::recover using recursion and intelligent system solving
 * @param noised: the noised signal
 * @return
 */

vec RDps::recover(const vec &noised)
{

    //denoiser the signal
    int n=noised.size();

    //denoising the condat
    vec denoised=denoiser->denoise(noised);
    saveSignal("condat",denoised);


    //eliminated indices
    for(int i=0;i<n-1;i++)
        if(denoised(i)!=denoised(i+1))
            possibleLP.insert(i);


    //compute the first Cholesky decomposition, and the initial status

    vecs E(n-1,0);
    C=chol_tridiagonal(main_matrix(E,n));

    vec X(n);
    int pos;
    std::queue<interval> Q ;
    Q.push(pair<int,int>(0,n-1));
    while(!Q.empty())
    {
        pair<int,int> Int=Q.front();Q.pop();
        int first=Int.first;
        int second=Int.second;

        if(split(noised,first,second,pos))
        {

            //add two intervals to the queue
            Q.push(pair<int,int>(first,pos));
            Q.push(pair<int,int>(pos+1,second));
        }
        else
        {

            vec X1=recover(noised,first,second);

            //flatting the part
            if(flatParts)
            {
                flatten(X1);
            }
            //replace the part of int with the signal
            for(int i=0;i<second-first+1;i++)
            {

                X[i+first]=X1[i];
            }
        }
    }


    return X;
}


double RDps::add_discontinuity(const vec &considered, int ind) const
{
    int n=considered.size();
    //parts of the signal
    //vec first(considered.begin(),considered.begin()+ind+1);
    //vec second(considered.begin()+ind+1,considered.end());
    vec first(ind+1);
    vec second(n-ind-1);
    for(int i=0;i<ind+1;i++)first(i)=considered(i);
    for(int i=0;i<n-ind-1;i++)second(i)=considered(ind+i+1);
    //vec secon(n-ind,&considered[ind]);
    //recovering each signal
    vec firstX=recover(considered,0,ind);
    vec secondX=recover(considered,ind+1,n-1);

    //computing the energies
    double ene1=energy(first,firstX,vecs(firstX.size()-1,0));
    double ene2=energy(second,secondX,vecs(secondX.size()-1,0));
    return ene1+ene2+alpha;

}

bool RDps::split(const vec &noised, int first, int second, int &pos)
{
    int n=second-first+1;                           //size of the given part to split
    vec part(n);
    for(int i=0;i<n;i++)part(i)=noised(first+i);

    //--------------------------------------------
    //    energy without any discontinuity
    //--------------------------------------------
    vec X=recover(noised,first,second);
    vecs E(n-1,0);
    double E_initial=energy(part,X,E);

    //looop over the possible line process
    priority_queue<lineEnergy> Queue;

    //loop over the possible line process
    for(int dis:possibleLP)
    {
        //checking if were in the interval
        if(dis>=first && dis<second)
        {
             //computing the associate energy
            double E_i=add_discontinuity(part,dis-first);
            if(E_i<E_initial)
            {
                Queue.emplace(dis,E_i);
            }
        }
    }

    //if queue is note empyt we found a good position for a discontinuity
    if(Queue.empty())
        return false;
    else
    {
        lineEnergy L=Queue.top();
        pos=L.lp;
        return true;
    }


    }

vec RDps::recover(const vec &noised,int first,int second) const
{
    if(first>second)
    {
        throw std::invalid_argument("Invalid interval argument to recover ");
        exit(1);
    }

    //size of the signal
    int n=second-first+1;

    //vec X(noised.begin()+first,noised.begin()+second+1);
    vec X(n);
    for(int i=0;i<n;i++) X(i)=noised(first+i);

    if(n==1)
    {
        X[0];
        return X;
    }
    double last_diagonal=sqrt((1+lam*lam)-C(1,n-2)*C(1,n-2));
    //initialization
    X[0]/=C(0,0);
    for(int i=1;i<n-1;i++)
    {
        X[i]=(X[i]-C(1,i-1)*X[i-1])/C(0,i);
    }
    //last operation
    X[n-1]=(X[n-1]-C(1,n-2)*X[n-2])/last_diagonal;

    //back_ward solve
    X[n-1]/=last_diagonal;
    for(int i=n-2;i>=0;i--)
    {
        X[i]-=C(1,i)*X[i+1];
        X[i]/=C(0,i);
    }

    return X;
}

void RDps::flatten(vec &N)
{
    //computing the mean
    double sum=0;
    int n=N.size();

    for(auto x: N)
        sum+=x;
    sum/=n;

    //replacting each instance with the mean
    for(auto &x: N)
        x=sum;

}

void RDps::setFlat(bool value)
{
    flatParts=value;
}


void RDps::setDenoiser(Condat_Denoiser *denoiser)
{
    this->denoiser=denoiser;
}

RDps::RDps(float l, float h1) : Dps(l, h1) {

    //initiating a condat denoiser
    denoiser=new Condat_Denoiser(l,0);        //bad adaptation to gnc

    //set of possible line processes
    possibleLP = std::set<int>();

}
