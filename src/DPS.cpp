#include "DPS.h"

vec chol_solve(const mat & M, const vec &b)
{
    vec x(b);
    return x;
}

mat Dps::main_matrix(vecs boolean_process,int n) const
{
    mat M(2,n,1);

    double lam2=lam*lam;
    for(int i=0;i<n-1;i++)
    {
        if(!boolean_process[i])
        {
            M(0,i)+=lam2;
            M(0,i+1)+=lam2;
            M(1,i)=-lam2;
        }
        else
            M(1,i)=0;
    }
    return M;
}


double Dps::energy(const vec noised, vec X, vecs E) const
{
    double ene=0.0;
    double lam2=lam*lam;
    int n=noised.size();
    for(int i=0;i<n;i++)
    {
        ene+=(noised(i)-X(i))*(noised(i)-X(i));
    }

    for(int i=0;i<n-1;i++)
    {
        if(!E[i])
            ene+=lam2*(X[i+1]-X[i])*(X[i+1]-X[i]);
        else
            ene+=alpha;
    }

    return ene;
}
vec Dps::signal_E(const vec &noised,vecs boolean_process) const
{
    int n=boolean_process.size()+1;
    auto M=main_matrix(boolean_process,n);
    return chol_solve(M,noised);
}


// the main function that check if the energy could be improved
bool Dps::min_level(const vec &noised, vec &X, vecs &E, double &min_energy) const
{
    //first energy and signal
    int ind=0;
    while(E[ind])ind++;

    vecs Ei=E;Ei[ind]=1;
    vec X1=signal_E(noised,Ei);
    double Emin=energy(noised,X1,Ei);
    //cout<<"min_energy"<<min_energy<<endl;
    //cout<<"starting energy"<<Emin<<endl;
    ind++;
    while(ind<E.size())
    {
       //cout<<"test for ind="<<ind<<endl;
       vecs Eind=E;Eind[ind]=1;
       vec Xint=signal_E(noised,Eind);
       double Ene_int=energy(noised,Xint,Eind);
       //cout<<"Energy is= "<<Ene_int<<endl;
       if(Ene_int<Emin)
       {
           Emin=Ene_int;
           X1=Xint;
           Ei=Eind;
       }
       ind++;
    }

    //verficiation if energy is updated;
    if(Emin<min_energy)
    {
        X=X1;
        min_energy=Emin;
        E=Ei;

        return true;
    }
    else
        return false;
}

vec Dps::recover(const vec &noised)
{
    int n=noised.size();
    //I try to write the subroutine using as much abstraction as possible

    //initiasing the boolean process
    vecs boolean_process(n-1,0);

    //initializing X as continuous signal corresponding to no discontinuity
    vec X=signal_E(noised,boolean_process);
    //computing the minimu
    double min_energy=energy(noised,X,boolean_process);

    //preparing the main loop of bpd
    vecs E=boolean_process;
    vec X_test;                    //signal to store the minimal signal in a given level;

    while(min_level(noised,X_test,E,min_energy))
    {
        //updating the boolean process
        boolean_process=E;
        X=X_test;

    }

    return X;
}


vec Dps::loadSignal(const string &filename)
{
    vec X;
    ifstream in(filename.c_str());
    if(in.is_open())
    {
        int n;
        in>>n;  X.resize(n);
            for(int i=0;i<n;i++)
            in>>X[i];
        in.close();
        return X;
    }
    else
    {
        cout<<"Could not find the file :"<<filename<<endl;
        exit(1);
    }
}

bool Dps::saveSignal(const string &filename, const vec &X)
{
    ofstream out(filename.c_str());
    if(out.is_open())
    {
        int n=X.size();
        out<<n<<endl;
        for(int i=0;i<n;i++)
            out<<X[i]<<endl;
        out.close();
        return true;
    }
    else
        return false;
}


void Dps::flatten(vec &N)
{
    //computing the mean
    double sum=0;
    int n=N.size();

    for(auto x: N)
        sum+=x;
    sum/=n;

    //replacting each instance with the mean
    for(auto &x: N)
        x=sum;

}


mat Dps::chol_tridiagonal(const mat &A)const {
    //check that A is a valid matrix
    if(A.size1()!=2)
    {
        throw std::invalid_argument("Matrix must be of size (2,n) representing the two columns");
    }

    int n=A.size2();
    mat C(A);

    //initialisat
    double valueP=C(0,0);
    if(valueP>=0)
        C(0,0)=sqrt(valueP);
    else
    {
        throw std::invalid_argument("Matrix is not semi definite positive");
    }
    C(1,0)/=C(0,0);

    //main loop
    for(int j=1;j<n;j++)
    {
        //L(1,j)=sqrt(L(1,j)-L(2,j-1)**2)
        valueP=C(0,j)-C(1,j-1)*C(1,j-1);
        if(valueP>=0)
            C(0,j)=sqrt(C(0,j)-C(1,j-1)*C(1,j-1));
        else
            throw std::invalid_argument("Matrix is not semi definite positive");
        //L(2,j)=L(2,j)/L(1,j)
        C(1,j)/=C(0,j);
    }
    return C;
}

vec Dps::band_fwsv(const mat &M, const vec &b) const{

    int n=b.size();
    vec x(b);
    //initialization
    x[0]/=M(0,0);
    for(int i=1;i<n;i++)
    {
        x[i]=(b[i]-M(1,i-1)*x[i-1])/M(0,i);
    }

    return x;
}

vec Dps::band_bwsv(const mat &M, const vec &b)const {
    int n=b.size();
    vec x(b);
    //initialization
    x[n-1]/=M(0,n-1);
    for(int i=n-2;i>=0;i--)
    {
        x[i]-=M(1,i)*x[i+1];
        x[i]/=M(0,i);
    }

    return x;
}

vec Dps::chol_solve(const mat &M, const vec &b)const {

    //computing cholesky decomposition
    auto C=chol_tridiagonal(M);
    //solve Cx1=b;
    auto x1=band_fwsv(C,b);


    //solving C^tx=x1;
    auto x2=band_bwsv(C,x1);


    return x2;
}

